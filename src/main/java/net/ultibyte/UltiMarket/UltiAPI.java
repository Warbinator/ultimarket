package net.ultibyte.UltiMarket;

import org.bukkit.Material;
import org.bukkit.entity.Player;

public final class UltiAPI {

	private UltiAPI(){}
	
	//Use camelcase for method names, especially in API's :)
	public static double getPrice(Material material, int amount){
		return UltiMarket.plugin.getPrice(material, amount);
	}
	
	public static void buy(Player player, Material material, int amount){
		UltiMarket.plugin.Buy(player, material, amount);
	}
	
	public static void sell(Player player, Material material, int amount){
		UltiMarket.plugin.Sell(player, material, amount);
	}
}
