package net.ultibyte.UltiMarket;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

public class UltiMarket extends JavaPlugin
{
	public static Economy econ = null;
	public static Permission perms = null;
	public static UltiMarket plugin;
	World world;
	HashMap<Material, Double> Market = new HashMap<Material, Double>();
	String currencyNamePlural;
	String currencyNameSingular;
	final double priceChangeAmount = getConfig().getDouble("price_change_per_buy_or_sell");
	final double minimumPrice = getConfig().getDouble("minimum_price");
	private static final Logger log = Logger.getLogger("Minecraft");
	List<Material> BLACKLIST = new ArrayList<Material>();

	// Random comment

	@Override
	public void onEnable()
	{
		plugin = this;
		if (!setupEconomy())
		{
			log.severe(String.format("UltiMarket has been disabled because you forgot to put Vault in! It's useless without it :(", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		currencyNamePlural = econ.currencyNamePlural();
		currencyNameSingular = econ.currencyNameSingular();
		loadConfig();
		try
		{
			MetricsLite metrics = new MetricsLite(this);
			metrics.start();
		} catch (IOException e)
		{
			getLogger().info("PluginMetrics failed to load, doesn't affect anything in UltiMarket though :P");
		}
		File f = new File(this.getDataFolder() + File.separator + "Market.ultifile");
		if (!f.exists())
		{
			createDatabaseFile();
		} else
		{
			Load(f);
		}
		try
		{
			List<Integer> IDBLACKLIST = this.getConfig().getIntegerList("Blacklist");
			for (int i : IDBLACKLIST)
			{
				if (ValidMaterial("" + i))
				{
					BLACKLIST.add(getMaterial("" + i));
				} else
				{
					getLogger().info("Item '" + i + "' NOT added to UltiMarket blacklist since it is not a valid material ID");
				}
			}
		} catch (Exception e)
		{
			getLogger().info("UltiMarket item Blacklist NOT loaded D:, probably formatted incorrectly! Use ITEM IDs!");
		}
		getLogger().info("UltiMarket loaded successfully! :D");
	}

	@Override
	public void onDisable()
	{
		getLogger().info("*UltiMarket waves as it fades off your server...*");
	}

	private void loadConfig()
	{
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

	private void createDatabaseFile()
	{
		File f = new File(this.getDataFolder() + File.separator + "Market.ulti");
		try
		{
			PopulateMarket();
			ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(f));
			o.writeObject(Market);
			o.close();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}

	private void Save()
	{
		File f = new File(this.getDataFolder() + File.separator + "Market.ultifile");
		try
		{
			ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(f));
			o.writeObject(Market);
			o.close();
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}

	private void Load(File f)
	{
		try
		{
			FileInputStream fis = new FileInputStream(f);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Market = (HashMap<Material, Double>) ois.readObject();
			ois.close();
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e1)
		{
			e1.printStackTrace();
		}
	}

	private boolean setupEconomy()
	{
		if (getServer().getPluginManager().getPlugin("Vault") == null)
		{
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null)
		{
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (sender instanceof Player)
		{
			Player player = (Player) sender;

			if (args.length == 2)
			{
				if (cmd.getName().equalsIgnoreCase("buy"))
				{
					if (ValidMaterial(args[0]) && (ValidAmount(args[1])))
					{
						Material material = getMaterial(args[0]);
						if (!BLACKLIST.contains(material))
						{
							int amount = Integer.parseInt(args[1]);
							Buy(player, material, amount);
						} else
						{
							player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
									+ "blacklisted " + ChatColor.GREEN + "on this server!");
						}
						return true;
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + "eg. '" + ChatColor.WHITE + "/" + ChatColor.RED
								+ "buy " + ChatColor.YELLOW + "rotten_flesh " + ChatColor.LIGHT_PURPLE + "3" + ChatColor.GREEN + "', for " + ChatColor.RED + "purchasing " + ChatColor.LIGHT_PURPLE
								+ "3 " + ChatColor.YELLOW + "Rotten Flesh.");
						return true;
					}
				} else if (cmd.getName().equalsIgnoreCase("sell"))
				{
					if (ValidMaterial(args[0]) && (ValidAmount(args[1])))
					{
						if (args[0].equalsIgnoreCase("hand"))
						{
							ItemStack item = player.getItemInHand();
							Material material = item.getType();
							if (!BLACKLIST.contains(material))
							{
								int amount = Integer.parseInt(args[1]);
								Sell(player, material, amount);
							} else
							{
								player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
										+ "blacklisted " + ChatColor.GREEN + "on this server!");
							}
							return true;
						} else
						{
							Material material = getMaterial(args[0]);
							if (!BLACKLIST.contains(material))
							{
								int amount = Integer.parseInt(args[1]);
								Sell(player, material, amount);
							} else
							{
								player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
										+ "blacklisted " + ChatColor.GREEN + "on this server!");
							}
							return true;
						}
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN
								+ "Something's wrong in what you typed! (maybe a missing '_'?) eg. '" + ChatColor.WHITE + "/" + ChatColor.GREEN + "sell " + ChatColor.YELLOW + "apple "
								+ ChatColor.LIGHT_PURPLE + "3" + ChatColor.GREEN + "', for " + ChatColor.GREEN + "selling " + ChatColor.LIGHT_PURPLE + "3 " + ChatColor.YELLOW + "apples.");
						return true;
					}
				} else if (cmd.getName().equalsIgnoreCase("price"))
				{
					if (ValidMaterial(args[0]) && (ValidAmount(args[1])))
					{
						Material material = getMaterial(args[0]);
						if (!BLACKLIST.contains(material))
						{
							double price = getPrice(material, Integer.parseInt(args[1]));
							player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.LIGHT_PURPLE + args[1] + " " + ChatColor.YELLOW
									+ getName(material) + ChatColor.GREEN + " costs " + ChatColor.GOLD + price + " " + ChatColor.AQUA + currencyNamePlural + ChatColor.GREEN + "!");
						} else
						{
							player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
									+ "blacklisted " + ChatColor.GREEN + "on this server!");
						}
						return true;
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN
								+ "Something's wrong in what you typed! (maybe a missing '_'?) eg. '" + ChatColor.WHITE + "/" + ChatColor.GOLD + "price " + ChatColor.YELLOW + "rotten_flesh "
								+ ChatColor.LIGHT_PURPLE + "3" + ChatColor.GREEN + "', for " + ChatColor.GREEN + "checking the " + ChatColor.GOLD + "price " + ChatColor.GREEN + "to" + ChatColor.RED
								+ " buy " + ChatColor.LIGHT_PURPLE + "3 " + ChatColor.YELLOW + "Rotten Flesh" + ChatColor.GREEN + ".");
						return true;
					}
				}
			} else if (args.length == 1)
			{
				if (cmd.getName().equalsIgnoreCase("buy"))
				{
					if (ValidMaterial(args[0]))
					{
						Material material = getMaterial(args[0]);
						if (!BLACKLIST.contains(material))
						{
							int amount = 1;
							Buy(player, material, amount);
						} else
						{
							player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
									+ "blacklisted " + ChatColor.GREEN + "on this server!");
						}
						return true;
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN
								+ "Something's wrong in what you typed! (maybe a missing '_'?) eg. '" + ChatColor.WHITE + "/" + ChatColor.RED + "buy " + ChatColor.YELLOW + "rotten_flesh"
								+ ChatColor.GREEN + "', for " + ChatColor.RED + "purchasing " + ChatColor.LIGHT_PURPLE + "1 " + ChatColor.YELLOW + "Rotten Flesh" + ChatColor.GREEN + ".");
						return true;
					}
				} else if (cmd.getName().equalsIgnoreCase("sell"))
				{
					if (ValidMaterial(args[0]) || args[0].equalsIgnoreCase("hand"))
					{
						if (args[0].equalsIgnoreCase("hand"))
						{
							ItemStack item = player.getItemInHand();
							Material material = item.getType();
							if (!BLACKLIST.contains(material))
							{
								int amount = item.getAmount();
								Sell(player, material, amount);
							} else
							{
								player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
										+ "blacklisted " + ChatColor.GREEN + "on this server!");
							}
							return true;
						} else
						{
							Material material = getMaterial(args[0]);
							if (!BLACKLIST.contains(material))
							{
								int amount = 1;
								Sell(player, material, amount);
							} else
							{
								player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
										+ "blacklisted " + ChatColor.GREEN + "on this server!");
							}
							return true;
						}
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN
								+ "Something's wrong in what you typed! (maybe a missing '_'?) eg. '" + ChatColor.WHITE + "/" + ChatColor.GREEN + "sell " + ChatColor.YELLOW + "rotten_flesh"
								+ ChatColor.GREEN + "', for " + ChatColor.GREEN + "selling " + ChatColor.LIGHT_PURPLE + "1 " + ChatColor.YELLOW + "Rotten Flesh.");
						return true;
					}
				} else if (cmd.getName().equalsIgnoreCase("price"))
				{
					if (ValidMaterial(args[0]))
					{
						Material material = getMaterial(args[0]);
						if (!BLACKLIST.contains(material))
						{
							double price = getPrice(material, 1);
							player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.LIGHT_PURPLE + "1" + " " + ChatColor.YELLOW
									+ getName(material) + ChatColor.GREEN + " costs " + ChatColor.GOLD + price + " " + ChatColor.AQUA + currencyNamePlural + ChatColor.GREEN + "!");
						} else
						{
							player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
									+ "blacklisted " + ChatColor.GREEN + "on this server!");
						}
						return true;
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN
								+ "Something's wrong in what you typed! (maybe a missing '_'?) eg. '" + ChatColor.WHITE + "/" + ChatColor.GOLD + "price " + ChatColor.YELLOW + "rotten_flesh"
								+ ChatColor.GREEN + "', for " + ChatColor.GREEN + "checking the " + ChatColor.GOLD + "price " + ChatColor.GREEN + "of" + ChatColor.YELLOW + "Rotten Flesh"
								+ ChatColor.GREEN + ".");
						return true;
					}
				}
			} else if (args.length == 0)
			{
				if (cmd.getName().equalsIgnoreCase("sellall"))
				{
					Inventory inventory = player.getInventory();
					if (inventory.getContents().length > 0)
					{
						for (ItemStack item : inventory.getContents())
						{
							if (item != null)
							{
								Material material = item.getType();
								if (!BLACKLIST.contains(material))
								{
									int amount = item.getAmount();
									Sell(player, material, amount);
								} else
								{
									player.sendMessage(ChatColor.BLACK + "[ " + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + " ] " + ChatColor.GREEN + getName(material) + " is " + ChatColor.RED
											+ "blacklisted " + ChatColor.GREEN + "on this server!");
								}
							}
						}
						return true;
					} else
					{
						player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN + "There are no sellable " + ChatColor.YELLOW + "items "
								+ ChatColor.GREEN + "in your " + ChatColor.YELLOW + "inventory" + ChatColor.GREEN + "!");
						return true;
					}
				}
			}
		}
		return false;
	}

	public void Buy(Player player, Material material, int amount)
	{
		double price = getPrice(material, amount);
		if (CanAfford(player, price))
		{
			ItemStack goods = new ItemStack(material, amount);
			player.getInventory().addItem(goods);
			TransactBuy(material, amount, player);
			player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.LIGHT_PURPLE + amount + " " + ChatColor.YELLOW + getName(material)
					+ ChatColor.RED + " bought " + ChatColor.GREEN + "for " + ChatColor.GOLD + price + " " + ChatColor.AQUA + currencyNamePlural + ChatColor.GREEN + "!");
		} else
		{
			double difference = getDifference(player, price);
			player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.GREEN + "You can't afford that! You need " + ChatColor.GOLD + difference
					+ ChatColor.GREEN + " more " + ChatColor.AQUA + currencyNamePlural + ChatColor.GREEN + "!");
		}
	}

	public void Sell(Player player, Material material, int amount)
	{
		double yield = getYield(material, amount, player);
		ItemStack item = new ItemStack(material);
		Inventory inventory = player.getInventory();
		if (inventory.containsAtLeast(item, amount))
		{
			ItemStack removeMe = new ItemStack(material, amount);
			inventory.removeItem(removeMe);
			TransactSell(material, amount, player);
			player.sendMessage(ChatColor.BLACK + "[" + ChatColor.AQUA + "UltiMarket" + ChatColor.BLACK + "] " + ChatColor.LIGHT_PURPLE + amount + " " + ChatColor.YELLOW + getName(material)
					+ ChatColor.GREEN + " sold " + ChatColor.GREEN + "for " + ChatColor.GOLD + yield + " " + ChatColor.AQUA + currencyNamePlural + ChatColor.GREEN + "!");
		}
	}

	public boolean ValidMaterial(String name)
	{
		if (Material.matchMaterial(name) != null)
		{
			return true;
		}
		return false;
	}

	public boolean ValidAmount(String amount)
	{
		try
		{
			Integer.parseInt(amount);
		} catch (Exception e)
		{
			return false;
		}
		return true;
	}

	public Material getMaterial(String name)
	{
		Material material = Material.matchMaterial(name);
		return material;
	}

	public double getPrice(Material material, int amount)
	{
		double price = Market.get(material);
		double totalPrice = 0.00;
		for (int n = 1; n <= amount; n++)
		{
			if (price <= minimumPrice)
			{
				totalPrice += minimumPrice;
			} else
			{
				totalPrice += price;
			}
			price += priceChangeAmount;
		}
		return Round(totalPrice);
	}

	public double getYield(Material material, int amount, Player player)
	{
		double price = Market.get(material);
		double yield = 0.00;
		price -= priceChangeAmount;
		for (int n = 1; n <= amount; n++)
		{
			if (price <= minimumPrice)
			{
				yield += minimumPrice;
			} else
			{
				yield += price;
			}
			price -= priceChangeAmount;
		}
		return Round(yield);
	}

	public void PopulateMarket()
	{
		for (Material material : Material.values())
		{
			Market.put(material, getConfig().getDouble("minimum_price"));
		}
	}

	public boolean CanAfford(Player player, double price)
	{
		if (econ.getBalance(player) >= price)
		{
			return true;
		}
		return false;
	}

	public void TransactBuy(Material material, int amount, Player player)
	{
		double price = Market.get(material);
		double totalPrice = 0.00;
		for (int n = 1; n <= amount; n++)
		{
			if (price <= minimumPrice)
			{
				totalPrice += minimumPrice;
			} else
			{
				totalPrice += price;
			}
			price += priceChangeAmount;
		}
		econ.withdrawPlayer(player, Round(totalPrice));
		Market.put(material, Round(price));
		Save();
	}

	public void TransactSell(Material material, int amount, Player player)
	{
		double price = Market.get(material);
		double yield = 0.00;
		price -= priceChangeAmount;
		for (int n = 1; n <= amount; n++)
		{
			if (price <= minimumPrice)
			{
				yield += minimumPrice;
			} else
			{
				yield += price;
			}
			price -= priceChangeAmount;
		}
		econ.depositPlayer(player, Round(yield));
		price += priceChangeAmount;
		Market.put(material, Round(price));
		Save();
	}

	public double Round(double d)
	{
		BigDecimal bd = new BigDecimal(Double.toString(d));
		bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public double getDifference(Player player, double price)
	{
		return Round(price - econ.getBalance(player));
	}

	public String getName(Material material)
	{
		String name = material.toString().toLowerCase();
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		return name;
	}

	public String NiceName(String name)
	{
		name = name.toLowerCase();
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		name = name.replace('_', ' ');
		for (int a = 0; a <= name.length() - 1; a++)
		{
			if (name.substring(a, ++a).equalsIgnoreCase(" "))
			{
				char[] letters = name.toCharArray();
				letters[a] = Character.toUpperCase(letters[a]);
				name = letters.toString();
			}
		}
		return name;
	}
}